package be.multimedi.testing.game;

import be.multimedi.testing.game.logic.Game;
import be.multimedi.testing.game.logic.GameLogic;

/**
 * @author Sven Wittoek
 * created on Wednesday, 2/10/2021
 */
public class Main {
    public static void main(String[] args) {
        Game game = new Game();
        game.play();
    }
}
