package be.multimedi.testing.game.characters;

import be.multimedi.testing.game.characters.exceptions.NotFoundException;
import be.multimedi.testing.game.characters.exceptions.TooLowNumberException;
import be.multimedi.testing.game.logic.util.RandomUtil;
import be.multimedi.testing.game.potions.Drinkable;
import be.multimedi.testing.game.potions.HealthPotion;

import java.util.*;

/**
 * @author Sven Wittoek
 * created on Friday, 2/5/2021
 */
public class Player implements Character{
    private String nickname;
    private int maxHealth;
    private int health;
    private int maxDamage;
    private int level;
    private Collection<Drinkable> availablePotions;
    private int numberOfUsedPotions;

    protected Player() {
        availablePotions = new ArrayList<>();
        setLevel(1);
    }

    public Player(String nickname, int maxHealth, int maxDamage) {
        this();
        setNickname(nickname);
        setMaxHealth(maxHealth);
        setHealth(maxHealth);
        setMaxDamage(maxDamage);
    }

    public Player(String nickname, int maxHealth, int maxDamage, int numberOfAvailablePotions) {
        this(nickname, maxHealth, maxDamage, 1, numberOfAvailablePotions);
    }

    public Player(String nickname, int maxHealth, int maxDamage, int level, int numberOfAvailablePotions) {
       this(nickname, maxHealth, maxDamage);
        setLevel(level);
        addPotions(numberOfAvailablePotions);
    }

    public Player(String nickname, int maxHealth, int maxDamage, int level, Collection<Drinkable> availablePotions) {
       this(nickname, maxHealth, maxDamage);
        setLevel(level);
        setAvailablePotions(availablePotions);
    }

    @Override
    public String getName() {
        return nickname;
    }

     void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

     void setMaxHealth(int maxHealth) {
        if (maxHealth <= 0) {
            throw new TooLowNumberException();
        }
        this.maxHealth = maxHealth;
    }

    @Override
    public int getHealth() {
        return health;
    }

     void setHealth(int health) {
        if (health <= 0) {
            throw new TooLowNumberException();
        }
        this.health = health;
    }

    public int getMaxDamage() {
        return maxDamage;
    }

     void setMaxDamage(int maxDamage) {
        if (maxDamage <= 0) {
            throw new TooLowNumberException();
        }
        this.maxDamage = maxDamage;
    }

    @Override
    public int getRandomDamage() {
        return RandomUtil.getRandomInt(1, getMaxDamage());
    }

    public int getLevel() {
        return level;
    }

     void setLevel(int level) {
        if (level <= 0) {
            throw new TooLowNumberException();
        }
        this.level = level;
    }

    public Collection<Drinkable> getAvailablePotions() {
        return availablePotions;
    }

     void setAvailablePotions(Collection<Drinkable> availablePotions) {
        this.availablePotions.clear();
        this.availablePotions.addAll(availablePotions);
    }

    public int getNumberOfUsedPotions() {
        return numberOfUsedPotions;
    }

     void setNumberOfUsedPotions(int numberOfUsedPotions) {
        if (numberOfUsedPotions < 0) {
            throw new TooLowNumberException();
        }
        this.numberOfUsedPotions = numberOfUsedPotions;
    }

    public void heal(int amount) {
        if (amount <= 0){
            throw new TooLowNumberException();
        }
        int newHealth = getHealth() + amount;
        setHealth(Math.min(newHealth, maxHealth));
    }

    @Override
    public void takeDamage(int amount) {
        if (amount <= 0) {
            throw new TooLowNumberException();
        }
        health = Math.max(0, getHealth() - amount);
    }

    public void addPotions(int amount) {
        if (amount <= 0){
            throw new TooLowNumberException();
        }
        for (int i = 0; i < amount; i++){
            availablePotions.add(HealthPotion.SMALL_HEALTH_POTION);
        }
    }

    public void addPotion(Drinkable potion) {
        availablePotions.add(potion);
    }

    public void throwAwayAllPotions(){
        availablePotions.clear();
    }

     void incrementNumberOfUsedPotions(){
        numberOfUsedPotions++;
    }

    public <T extends Drinkable> Drinkable takeOutPotion(Drinkable drinkable) {
        if (!availablePotions.contains(drinkable)){
            throw new NotFoundException();
        }
        incrementNumberOfUsedPotions();
        availablePotions.remove(drinkable);
        return drinkable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return maxHealth == player.maxHealth && health == player.health && maxDamage == player.maxDamage && level == player.level && numberOfUsedPotions == player.numberOfUsedPotions && Objects.equals(nickname, player.nickname) && Objects.equals(availablePotions, player.availablePotions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nickname, maxHealth, health, maxDamage, level, availablePotions, numberOfUsedPotions);
    }
}
