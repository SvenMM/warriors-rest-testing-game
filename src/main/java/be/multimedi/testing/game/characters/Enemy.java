package be.multimedi.testing.game.characters;

import be.multimedi.testing.game.characters.exceptions.TooLowNumberException;
import be.multimedi.testing.game.logic.util.RandomUtil;

import java.util.Random;

/**
 * @author Sven Wittoek
 * created on Friday, 2/5/2021
 */
public class Enemy implements Character{
    private int health;
    private int maxDamage;
    private String type;
    private int level;

    protected Enemy() {
    }

    public Enemy(EnemyType enemyType, int level) {
        this(enemyType.getHealth(), enemyType.getMaxDamage(), enemyType.name(), level);
    }

    public Enemy(int health, int maxDamage, String type, int level) {
        setHealth(health);
        setMaxDamage(maxDamage);
        setType(type);
        setLevel(level);
    }

    @Override
    public int getHealth() {
        return health;
    }

     void setHealth(int health) {
        if (health <= 0){
            throw new TooLowNumberException();
        }
        this.health = health;
    }

    @Override
    public int getRandomDamage() {
        return RandomUtil.getRandomInt(1, getMaxDamage());
    }

    public int getMaxDamage() {
        return maxDamage;
    }

     void setMaxDamage(int maxDamage) {
        if (maxDamage <= 0 ) {
            throw new TooLowNumberException();
        }
        this.maxDamage = maxDamage;
    }

    @Override
    public String getName() {
        return type;
    }

     void setType(String type) {
        this.type = type;
    }

    public int getLevel() {
        return level;
    }

     void setLevel(int level) {
        if (level <= 0) {
            throw new TooLowNumberException();
        }
        this.level = level;
    }

    @Override
    public void takeDamage(int amount) {
        if (amount <= 0) {
            throw new TooLowNumberException();
        }
        health = Math.max(0, getHealth() - amount);
    }
}
