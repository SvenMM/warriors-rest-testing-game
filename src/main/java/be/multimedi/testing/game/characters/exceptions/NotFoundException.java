package be.multimedi.testing.game.characters.exceptions;

/**
 * @author Sven Wittoek
 * created on Tuesday, 2/9/2021
 */
public class NotFoundException extends RuntimeException{
    public NotFoundException() {
    }

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotFoundException(Throwable cause) {
        super(cause);
    }
}
