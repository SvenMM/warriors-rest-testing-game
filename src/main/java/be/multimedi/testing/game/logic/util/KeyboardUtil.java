package be.multimedi.testing.game.logic.util;

import java.util.Scanner;

/**
 * @author Sven Wittoek
 * created on Wednesday, 18/05/2022
 */
public class KeyboardUtil {
    private static Scanner scanner = new Scanner(System.in);

    public static String askForText(String message) {
        System.out.println(message);
        return scanner.nextLine();
    }

    public static int askForNumber(String message) {
        System.out.println(message);
        Integer number = null;
        while (number == null) {
            try {
                number = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException nfe) {
                System.out.println("Please enter a number!");
            }
        }
        return number;
    }

    public static int askForNumber(String message, int lowerBound, int upperBound) {
        System.out.println(message);
        Integer number = null;
        while (number == null || number < lowerBound || number > upperBound) {
            try {
                number = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException nfe) {
                System.out.println("Please enter a number between " + lowerBound +
                        " and " + upperBound);
            }
        }
        return number;
    }

}
