package be.multimedi.testing.game.logic.util;

import java.util.Random;

/**
 * @author Sven Wittoek
 * created on Wednesday, 18/05/2022
 */
public class RandomUtil {

    private RandomUtil() {
    }

    public static int getRandomInt(int lowerBound, int upperBound) {
        Random random = new Random();
        return random.nextInt((upperBound + 1) - lowerBound) + lowerBound;
    }
}
