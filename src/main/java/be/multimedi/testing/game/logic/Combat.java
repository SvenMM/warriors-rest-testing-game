package be.multimedi.testing.game.logic;

import be.multimedi.testing.game.characters.Character;
import be.multimedi.testing.game.logic.exceptions.NotAliveException;

/**
 * @author Sven Wittoek
 * created on Wednesday, 2/10/2021
 */
public class Combat {
    static Combat combat;

    private Combat() {
    }

    public static Combat getInstance() {
        if (combat == null) {
            combat = new Combat();
        }
        return combat;
    }

    public void fight(Character characterToDealDamage, Character characterToReceiveDamage){
        checkIfCharacterIsAlive(characterToDealDamage);
        checkIfCharacterIsAlive(characterToReceiveDamage);

        int damage = characterToDealDamage.getRandomDamage();
        characterToReceiveDamage.takeDamage(damage);

        System.out.printf("%1$s dealt %2$d damage to %3$s. %nLeaving %3$s with %4$d health left.%n", characterToDealDamage.getName(),
                damage, characterToReceiveDamage.getName(), characterToReceiveDamage.getHealth());
    }

    private void checkIfCharacterIsAlive(Character character) {
        if (character.getHealth() <= 0) {
            throw new NotAliveException();
        }
    }
}
