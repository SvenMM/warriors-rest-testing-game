package be.multimedi.testing.game.logic;

import be.multimedi.testing.game.characters.Player;
import be.multimedi.testing.game.logic.util.KeyboardUtil;

import java.util.Scanner;

/**
 * @author Sven Wittoek
 * created on Wednesday, 2/10/2021
 */
public class Game {
    private Player player;
    private int choice;
    private GameLogic gameLogic;

    public Game() {

    }

    Game(Player player, GameLogic gameLogic) {
        this.player = player;
        this.gameLogic = gameLogic;
    }

    public Player getPlayer() {
        return player;
    }

    void setPlayer(Player player) {
        this.player = player;
    }

    public int getChoice() {
        return choice;
    }

    void setChoice(int choice) {
        this.choice = choice;
    }

    public GameLogic getGameLogic() {
        return gameLogic;
    }

    public void setGameLogic(GameLogic gameLogic) {
        this.gameLogic = gameLogic;
    }

    public void play() {
        askChoice();

        while (choice != 3) {
            if (choice == 1) {

                startUpGame();

                startFightingSequence();

                closeDownGame();
            }
            else if (choice == 2) {
                showLeaderboard();
            }

        }

    }

    private void showLeaderboard() {
        throw new UnsupportedOperationException("This operation is still a work in progress");
    }

    private void askChoice() {
        String message = String.format("What would you like to do?%n" +
                "1. Start game %n" +
                "2. See high scores %n" +
                "3. Quit game %n");

        choice = KeyboardUtil.askForNumber(message, 1, 3);
    }

    private void startFightingSequence() {
        choice = 0;
        while (player.getHealth() > 0 && choice != 2) {
            gameLogic.commenceCombat();
            if (player.getHealth() > 0) {
                askDecision();

                if (choice == 1) {
                    System.out.println("You dive deeper into the dungeon, hoping to find a brilliant treasure.");
                }
            }
        }
    }

    void askDecision() {
        String message = String.format("What would you like to do? %n" +
                "1. Explore further %n" +
                "2. Escape dungeon");
        choice = KeyboardUtil.askForNumber(message, 1, 2);
    }

    void closeDownGame() {
        if (player.getHealth() <= 0) {
            System.out.println("That last attack was way more than you can take and you collapse to the floor of the " +
                    "dungeon, thinking you just took your last breath.");
            System.out.println("You blink one last time before everything goes black and you see the enemy drawing his" +
                    " weapon for a last time. You turn your head away and feel warm blood spreading over your body.");
        } else {
            System.out.println("You managed to find the exit and you leave the dungeon with some extra gold, making plans" +
                    " to spend it al at the local inn.");
        }
        askChoice();
    }

    void startUpGame() {
        if (player == null) {
            String message = "What is your name?";
            String name = KeyboardUtil.askForText(message);
            this.player = new Player(name, 120, 50, 5);
            gameLogic = new GameLogic(player);
        } else {
            player.heal(player.getMaxHealth());
            player.throwAwayAllPotions();
            player.addPotions(5);
        }
        System.out.println("You enter a cave and get attacked");
    }
}
