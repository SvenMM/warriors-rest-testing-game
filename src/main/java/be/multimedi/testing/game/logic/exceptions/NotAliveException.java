package be.multimedi.testing.game.logic.exceptions;

/**
 * @author Sven Wittoek
 * created on Wednesday, 2/10/2021
 */
public class NotAliveException extends RuntimeException{
    public NotAliveException() {
    }

    public NotAliveException(String message) {
        super(message);
    }

    public NotAliveException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotAliveException(Throwable cause) {
        super(cause);
    }
}
